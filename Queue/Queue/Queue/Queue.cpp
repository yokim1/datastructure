﻿// Queue.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include <iostream>
using namespace std;

//create()
//init()
//is_empty()
//is_full()
//enqueue()
//dequeue()
//peek

//template<typename T>
struct Node {
    int data;
    
    Node* prev;
    Node* next;
};
Node* front = nullptr;
Node* tail = nullptr;
int maxSize = 0;
int currentSize = 0;

void initialize() {
    
    cin >> maxSize;
    currentSize = 0;
}

bool isEmpty() {
    if (currentSize == 0) {
        return true;
    }

    return false;
}

bool isFull() {
    if (currentSize == maxSize) {
        return true;
    }

    return false;
}

void enqueue(int data) {
    if (isFull() == true) {
        return;
    }

    if (front == nullptr) {
        Node* node = new Node;
        node->data = data;
        node->prev = nullptr;
        node->next = nullptr;

        front = node;
        tail = node;

        ++currentSize;
        return;
    }

    Node* node = new Node;
    node->data = data;
    node->prev = tail;
    node->next = nullptr;
    tail->next = node;
    tail = node;
    ++currentSize;
}

void dequeue() {
    if (isEmpty() == true) {
        return;
    }

    front = front->next;
    delete front->prev;
    front->prev = nullptr;
    --currentSize;
}

int peek() {
    return front->data;
}

int main()
{
    initialize();
    enqueue(1);
    enqueue(2);
    enqueue(3);
    enqueue(4);
    enqueue(5);
    enqueue(6);
    enqueue(7);

    cout << peek() << endl;

    dequeue();
    dequeue();

    cout << peek() << endl;

    return 0;
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
