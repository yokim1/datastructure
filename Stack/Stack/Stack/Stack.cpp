﻿// Stack.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include <iostream>
using namespace std;

// is_full
// is_empty
// push
// pop
// peek

struct Node {
    int data;
    
    Node* next;
    Node* prev;
};

Node* top;
int maxSize;
int currentSize;

void initialization() {
    cout << "size of stack: ";
    cin >> maxSize;
    currentSize = 0;
    top = nullptr;
}

bool is_full() {
    if (currentSize == maxSize) {
        return true;
    }
    return false;
}

bool is_emptys() {
    if (currentSize == 0) {
        return true;
    }
    return false;
}

void push(int item) {
    
    if (top == nullptr) {
        Node* ptr = new Node;
        ptr->next = nullptr;
        ptr->prev = nullptr;
        top = ptr;
        ptr->data = item;
        ++currentSize;
        return;
    }
    
    Node* ptr = new Node;
    ptr->next = nullptr;
    ptr->prev = top;
    top->next = ptr;
    top = ptr;
    ptr->data = item;
    ++currentSize;
}

void pop() {
    if (top->prev == nullptr) {
        top = nullptr;
        --currentSize;
        return;
    }

    top = top->prev;
    top->next = nullptr;
    --currentSize;
}

int peek() {
    return top->data;
}

int main()
{
    initialization();
    
    int input;
    for (int i = 0; i < maxSize; ++i) {
        cin >> input;

        push(input);
    }

    cout << is_emptys() << endl;
    cout<<peek()<<endl;
    pop();
    cout << peek() << endl;

    return 0;
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
