﻿// BinarySearchTree.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include <iostream>

using namespace std;

//insert()
//delete()
//search()
//min_value()
//inorderPrint()
//preorderPrint()
//postorderPrint()

typedef struct Node {
    int data;

    Node* left;
    Node* right;
}TreeNode;

TreeNode* root;

void Initialize() {
    root = nullptr;
}

TreeNode* InsertNode(TreeNode* node, int data) {
    if (node == nullptr) {
        TreeNode* newNode = new TreeNode;
        newNode->data = data;
        newNode->left = nullptr;
        newNode->right = nullptr;
        return newNode;
    }

    if (data < node->data) {
        node->left = InsertNode(node->left, data);
    }
    else if (node->data < data) {
        node->right = InsertNode(node->right, data);
    }

    return node;
}

TreeNode* minValue(TreeNode* node) {

    TreeNode* current = node;
    while (current->left == nullptr) {
        current = current->left;
    }
    return current;
}

TreeNode* deleteNodeHelper(TreeNode* node, int data);

void deleteNode(TreeNode* node, int data) {
    deleteNodeHelper(node, data);
}

TreeNode* deleteNodeHelper(TreeNode* node, int data) {
    if (node->data == data) {
        // 2 nodes
        if (node->left == nullptr && node->right == nullptr) {
            TreeNode* successor = minValue(node->right);
            node->data = successor->data;
            delete successor;
            successor = nullptr;
        }
        // only left node exist
        else if (node->left != nullptr && node->right == nullptr) {
            TreeNode* left = node->left;
            delete node;
            node = nullptr;
            node = left;
        }
        // right node exist
        else if (node->left == nullptr && node->right != nullptr) {
            TreeNode* right = node->right;
            delete node;
            node = nullptr;
            node = right;
        }
        // no node
        else if (node->left == nullptr && node->right == nullptr) {
            delete node;
            node = nullptr;
        }
    }
    else if(data < node->data ){
        deleteNodeHelper(node->left, data);
    }
    else if (node->data < data) {
        deleteNodeHelper(node->right, data);
    }

    return node;
}

bool searchNode(TreeNode* node, int data) {
    if (node == nullptr) {
        return false;
    }

    if (node->data == data) {
        return true;
    }

    if (data < node->data) {
        searchNode(node->left, data);
    }
    else if (node->data < data) {
        searchNode(node->right, data);
    }
}

void InorderPrint(TreeNode* node) {
    if (node != NULL) {
        InorderPrint(node->left);
        cout << node->data << endl;
        InorderPrint(node->right);
    }
}

void PreorderPrint(TreeNode* node) {
    if (node != NULL) {
        cout << node->data << endl;
        InorderPrint(node->left);
        InorderPrint(node->right);
    }
}

void PostorderPrint(TreeNode* node) {
    if (node != NULL) {
        InorderPrint(node->left);
        InorderPrint(node->right);
        cout << node->data << endl;
    }
}

int main()
{
    root = InsertNode(root, 30);
    root = InsertNode(root, 20);
    root = InsertNode(root, 10);
    root = InsertNode(root, 40);
    root = InsertNode(root, 50);
    root = InsertNode(root, 60);

    cout<<searchNode(root, 30)<<endl;
    cout << searchNode(root, 100) << endl;
    cout << searchNode(root, 20) << endl;
    cout << searchNode(root, 10) << endl;
    cout << searchNode(root, 40) << endl;
    cout << searchNode(root, 80) << endl;
    cout << searchNode(root, 90) << endl;

    root = InsertNode(root, 25);
    root = InsertNode(root, 35);

    InorderPrint(root);
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
